package annuaire;

import java.util.ArrayList;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;

import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamReader;

public class Annuaire {
	private ArrayList<Contact> monannuaire = new ArrayList<Contact>();

	public Annuaire() {
	}

	public void ajouter(Contact c) {
		monannuaire.add(c);
	}

	public void retirer(int index) {
		monannuaire.remove(index);
	}

	public void sauvegarderAnnuaire() {

		System.out.println("sauvegarde de l'annuaire");

		XMLOutputFactory xof = XMLOutputFactory.newInstance();

		try (FileOutputStream fos = new FileOutputStream(new File("contacts.xml"))) {

			XMLStreamWriter xsw = xof.createXMLStreamWriter(fos, "UTF-8");
			xsw.writeStartDocument("UTF-8", "1.0");

			xsw.writeStartElement("contacts");
			for (Contact c : this.getMonannuaire()) {
				xsw.writeStartElement("contact");
				xsw.writeStartElement("nom");
				xsw.writeCharacters(c.getNom());
				xsw.writeEndElement();

				xsw.writeStartElement("prenom");
				xsw.writeCharacters(c.getPrenom());
				xsw.writeEndElement();

				xsw.writeStartElement("adresse");
				xsw.writeCharacters(c.getAdresse());
				xsw.writeEndElement();

				xsw.writeStartElement("email");
				xsw.writeCharacters(c.getEmail());
				xsw.writeEndElement();

				xsw.writeStartElement("commentaire");
				xsw.writeCharacters(c.getCommentaire());
				xsw.writeEndElement();

				xsw.writeStartElement("num");
				xsw.writeCharacters(c.getNum());
				xsw.writeEndElement();
				xsw.writeEndElement();
			}

			xsw.writeEndElement();
			xsw.flush();
			xsw.close();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (XMLStreamException e) {
			e.printStackTrace();
		}
	}

	public void restaurerAnnuaire() {
		System.out.println("restauration de l'annuraire");
		String fileName = "contacts.xml";
		XMLInputFactory factory = XMLInputFactory.newInstance();
		File file = new File(fileName);
		Contact c = new Contact();

		try {
			XMLStreamReader reader = factory.createXMLStreamReader(new FileReader(file));
			while (reader.hasNext()) {
				int type = reader.next();
				switch (type) {
				case XMLStreamReader.START_ELEMENT:

					String name = reader.getLocalName();
					switch (name) {
					case "contacts":
						System.out.println("Ouverture balise contacts");

						break;

					case "contact":
						break;

					case "nom":
						c.setNom(reader.getElementText());
						break;

					case "prenom":
						c.setPrenom(reader.getElementText());
						break;

					case "adresse":
						c.setAdresse(reader.getElementText());
						break;

					case "email":
						c.setEmail(reader.getElementText());
						break;

					case "commentaire":
						System.out.println("commentaire:");
						c.setCommentaire(reader.getElementText());
						break;

					case "num":
						c.setNum(reader.getElementText());
					}

					break;

				case XMLStreamReader.END_ELEMENT:
					name = reader.getLocalName();
					switch (name) {
					case "contact":
						this.monannuaire.add(c);
						c = new Contact();
						break;

					}

					break;

				case XMLStreamReader.CHARACTERS:
					if (!reader.isWhiteSpace()) {
					}
					break;

				}
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (XMLStreamException e) {
			e.printStackTrace();
		}
	}

	public ArrayList<Contact> getMonannuaire() {
		return monannuaire;
	}

	public ArrayList<Contact> getAnnuairede(String utilisateur) {
		System.out.println("Getannuaire de: " + utilisateur);
		ArrayList<Contact> resultat = new ArrayList<Contact>();
		for (Contact c : monannuaire) {
			if (c.getAppartenance().equals(utilisateur)) {
				resultat.add(c);
			}
		}
		return resultat;
	}

	public int rechercherContact(Contact ct) {
		int result = -1;
		while (!monannuaire.get(++result).getNum().equals(ct.getNum())) {
		}
		return result;
	}

	public boolean existe(Contact ct) {
		boolean result = false;
		for (Contact c : getMonannuaire()) {
			if (c.getNum().equals(ct.getNum())) {
				result = true;
			}
		}
		return result;

	}

	public void supprimerContact(Contact ct) {
		if (existe(ct)) {
			int index = rechercherContact(ct);
			getMonannuaire().remove(index);
		}
	}
	
	public ArrayList<Contact> filtrecontacts(String filtre){
		ArrayList<Contact> result = new ArrayList<Contact>();
		for (Contact c : getMonannuaire()) {
			if(
//					par défaut recherche sur tout les attribut
					c.getNom().contains(filtre) || 
					c.getPrenom().contains(filtre)|| 
					c.getNum().contains(filtre) ||
					c.getAdresse().contains(filtre)||
					c.getEmail().contains(filtre) ||
					c.getCommentaire().contains(filtre)
					
					) {
				result.add(c);
			}
		}
		
		return result;
	}
	
	public ArrayList<Contact> filtrecontactsNom(String filtre){
		ArrayList<Contact> result = new ArrayList<Contact>();
		for (Contact c : getMonannuaire()) {
			if(c.getNom().contains(filtre)) {
				result.add(c);
			}
		}
		
		return result;
	}
	
	public ArrayList<Contact> filtrecontactsPrenom(String filtre){
		ArrayList<Contact> result = new ArrayList<Contact>();
		for (Contact c : getMonannuaire()) {
			if(c.getPrenom().contains(filtre)) {
				result.add(c);
			}
		}
		
		return result;
	}
	
	public ArrayList<Contact> filtrecontactsNum(String filtre){
		ArrayList<Contact> result = new ArrayList<Contact>();
		for (Contact c : getMonannuaire()) {
			if(c.getNum().contains(filtre)) {
				result.add(c);
			}
		}
		
		return result;
	}
	
	public ArrayList<Contact> filtrecontactsEmail(String filtre){
		ArrayList<Contact> result = new ArrayList<Contact>();
		for (Contact c : getMonannuaire()) {
			if(c.getEmail().contains(filtre)) {
				result.add(c);
			}
		}
		
		return result;
	}
	
	public ArrayList<Contact> filtrecontactsCommentaire(String filtre){
		ArrayList<Contact> result = new ArrayList<Contact>();
		for (Contact c : getMonannuaire()) {
			if(c.getCommentaire().contains(filtre)) {
				result.add(c);
			}
		}
		
		return result;
	}
	
	public ArrayList<Contact> filtrecontactsAdresse(String filtre){
		ArrayList<Contact> result = new ArrayList<Contact>();
		for (Contact c : getMonannuaire()) {
			if(c.getAdresse().contains(filtre)) {
				result.add(c);
			}
		}
		
		return result;
	}

}
