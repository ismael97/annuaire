package annuaire;

public class Contact {
	private String nom;
	private String prenom;
	private String num;
	private String adresse;
	private String email;
	private String commentaire;
	private String appartenance;

	public Contact() {
		super();
	}

	public Contact(String nom, String prenom, String adresse, String email, String Commentaire, String num) {
		this.setAdresse(adresse);
		this.setCommentaire(Commentaire);
		this.setEmail(email);
		this.setNum(num);
		this.setPrenom(prenom);
		this.setNom(nom);
		this.setAppartenance("VIDE");

	}

	public Contact(String nom, String prenom, String adresse, String email, String Commentaire, String num,
			String appartenance) {
		this.setAdresse(adresse);
		this.setCommentaire(Commentaire);
		this.setEmail(email);
		this.setNum(num);
		this.setPrenom(prenom);
		this.setNom(nom);
		this.setAppartenance(appartenance);

	}

	// Autres constructeurs ?

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getNum() {
		return num;
	}

	public void setNum(String num) {
		this.num = num;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCommentaire() {
		return commentaire;
	}

	public void setCommentaire(String commentaire) {
		this.commentaire = commentaire;
	}

	public String getAppartenance() {
		return appartenance;
	}

	public void setAppartenance(String appartenance) {
		this.appartenance = appartenance;
	}
}
