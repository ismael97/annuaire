package com;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import annuaire.Annuaire;
import annuaire.Contact;

@WebServlet("/Acceuil")
public class Acceuil extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private Annuaire annuaire;

	public void init() {
		annuaire = new Annuaire();
		this.annuaire.restaurerAnnuaire();
		ServletContext context = this.getServletContext();
		context.setAttribute("annuaire", annuaire);

	}

	public void connexionutilisateur(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		RequestDispatcher dis;
		dis = request.getRequestDispatcher("/menu.jsp");
		dis.forward(request, response);

	}

	public void destroy() {
		this.annuaire.sauvegarderAnnuaire();
	}

	public Contact creercontact(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String nom, prenom, num, adresse, email, commentaire;
		nom = request.getParameter("nom");
		prenom = request.getParameter("prenom");
		num = request.getParameter("num");
		adresse = request.getParameter("adresse");
		email = request.getParameter("email");
		commentaire = request.getParameter("commentaire");
		Contact ct = new Contact(nom, prenom, adresse, email, commentaire, num);
		return ct;

	}

	public Contact anciencontact(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String nom, prenom, num, adresse, email, commentaire;
		nom = request.getParameter("oldnom");
		prenom = request.getParameter("oldprenom");
		num = request.getParameter("oldnum");
		adresse = request.getParameter("oldadresse");
		email = request.getParameter("oldemail");
		commentaire = request.getParameter("oldcommentaire");
		Contact ct = new Contact(nom, prenom, adresse, email, commentaire, num);
		return ct;

	}

	public void ajoutercontact(Contact ct) {

		this.annuaire.ajouter(ct);

	}

	public boolean existeDanslabase(Contact c) {
		boolean b = false;

		for (Contact ct : this.annuaire.getMonannuaire()) {
			if (ct.getNum().equals(c.getNum())) {
				b = true;
				break;
			}
		}

		return b;

	}

	public void supressioncontact(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Contact ctasupprimer = creercontact(request, response);
		if (annuaire.existe(ctasupprimer)) {
			annuaire.supprimerContact(ctasupprimer);
			RequestDispatcher dis = request.getRequestDispatcher("/confirmationsupression.jsp");
			dis.forward(request, response);
		} else {
//    		Lorsque le contact n'existe pas ou plus dans la base
			System.out.println("non trouvé");
		}
	}

	public void modificationcontact(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		int index = Integer.parseInt(request.getParameter("idcontactamodifier"));
		request.setAttribute("indexcontactasupprimer", index);
		Contact c = creercontact(request, response);

		RequestDispatcher dis;
		if (annuaire.existe(c)) {
			request.setAttribute("contactamodifier", c);
			dis = request.getRequestDispatcher("/modificationcontact.jsp");

		} else {
			System.out.println("Le contact n'existe pas");
			dis = request.getRequestDispatcher("/erreur.jsp");
		}
		dis.forward(request, response);

	}

	public void filtrecontact(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String filtre = request.getParameter("filtre");

		String type = request.getParameter("typerecherche");
		ArrayList<Contact> resultatrecherche = new ArrayList<Contact>();

		switch (type) {
		case "tous":
			resultatrecherche = annuaire.filtrecontacts(filtre);
			break;
		case "nom":
			resultatrecherche = annuaire.filtrecontactsNom(filtre);
			break;
		case "prenom":
			resultatrecherche = annuaire.filtrecontactsPrenom(filtre);
			break;

		case "num":
			resultatrecherche = annuaire.filtrecontactsNum(filtre);
			break;

		case "adresse":
			resultatrecherche = annuaire.filtrecontactsAdresse(filtre);
			break;

		case "commentaire":
			resultatrecherche = annuaire.filtrecontactsCommentaire(filtre);
			break;

		case "email":
			resultatrecherche = annuaire.filtrecontactsEmail(filtre);
			break;

		}
		request.setAttribute("resultatrecherche", resultatrecherche);
		RequestDispatcher dis = request.getRequestDispatcher("/recherche.jsp");
		dis.forward(request, response);

	}

	public Acceuil() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		connexionutilisateur(request, response);

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String requete = request.getParameter("typerequete");
		if (requete != null) {
			RequestDispatcher dis;
			switch (requete) {
			case "creationcontact":
				Contact ct1 = creercontact(request, response);
				if (this.existeDanslabase(ct1)) {
					dis = request.getRequestDispatcher("/erreur.jsp");
					dis.forward(request, response);

				} else {
					this.ajoutercontact(ct1);
					dis = request.getRequestDispatcher("/confirmationajout.jsp");
					dis.forward(request, response);
				}

				break;

			case "supressioncontact":
				this.supressioncontact(request, response);
				break;

			case "modificationcontact":
				this.modificationcontact(request, response);
				break;

			case "validermodification":
				Contact ancienct = anciencontact(request, response);
				Contact ct2 = creercontact(request, response);
				if (annuaire.existe(ancienct)) {
					annuaire.supprimerContact(ancienct);
				}
				this.ajoutercontact(ct2);
				dis = request.getRequestDispatcher("/confirmationmodification.jsp");
				dis.forward(request, response);

				break;

			case "recherchecontact":
				filtrecontact(request, response);
				break;

			}
		} else {
			System.out.println("Aucune requete");
			RequestDispatcher dis = request.getRequestDispatcher("/menu.jsp");
			dis.forward(request, response);
		}
	}

}
