<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<meta charset="UTF-8">
<link href="styles/main.css" rel="stylesheet">
<title>Resultat recherche</title>
</head>
<body>

	<div class="menu_bar">
		<ul>
			<li><a href="http://localhost:8080/annuairepw/Acceuil">Acceuil</a>
		</ul>
	</div>
	<div class="contacts" id="contacts">
		<c:choose>
			<c:when test="${empty resultatrecherche}">
				<p>Le résultat de la recherche est vide</p>
			</c:when>
			<c:when test="${!empty resultatrecherche}">
				<div>


					<table>
						<tr>
							<th>index</th>
							<th>Nom</th>
							<th>Prenom</th>
							<th>Numéro</th>
							<th>Adresse</th>
							<th>Email</th>
							<th>Commentaire</th>

						</tr>
						<c:forEach items="${resultatrecherche}" var="contact"
							varStatus="count">
							<tr>
								<td><c:out value="${count.count}" /></td>
								<td><c:out value="${contact.getNom()}"></c:out></td>
								<td><c:out value="${contact.getPrenom()}"></c:out></td>
								<td><c:out value="${contact.getNum()}"></c:out></td>
								<td><c:out value="${contact.getAdresse()}"></c:out></td>
								<td><c:out value="${contact.getEmail()}"></c:out></td>
								<td><c:out value="${contact.getCommentaire()}"></c:out></td>
								<td>
									<form action="http://localhost:8080/annuairepw/Acceuil"
										method="post">
										<input type="submit" value="x" /> <input type="hidden"
											name="nom"
											value="<c:out value="${contact.getNom()}"></c:out>" /> <input
											type="hidden" name="prenom"
											value="<c:out value="${contact.getPrenom()}"></c:out>" /> <input
											type="hidden" name="num"
											value="<c:out value="${contact.getNum()}"></c:out>" /> <input
											type="hidden" name="adresse"
											value="<c:out value="${contact.getAdresse()}"></c:out> " />
										<input type="hidden" name="email"
											value="<c:out value="${contact.getEmail()}"></c:out>" /> <input
											type="hidden" name="commentaire"
											value="<c:out value="${contact.getCommentaire()}"></c:out>" />
										<input type="hidden" name="typerequete"
											value="supressioncontact" /> <input type="hidden"
											name="idcontactasupprimer"
											value="<c:out value="${count.index}"/>" />
									</form>
								</td>
								<td>
									<form action="http://localhost:8080/annuairepw/Acceuil"
										method="post">
										<input type="submit" value="m" /> <input type="hidden"
											name="nom"
											value="<c:out value="${contact.getNom()}"></c:out>" /> <input
											type="hidden" name="prenom"
											value="<c:out value="${contact.getPrenom()}"></c:out>" /> <input
											type="hidden" name="num"
											value="<c:out value="${contact.getNum()}"></c:out>" /> <input
											type="hidden" name="adresse"
											value="<c:out value="${contact.getAdresse()}"></c:out> " />
										<input type="hidden" name="email"
											value="<c:out value="${contact.getEmail()}"></c:out>" /> <input
											type="hidden" name="commentaire"
											value="<c:out value="${contact.getCommentaire()}"></c:out>" />
										<input type="hidden" name="typerequete"
											value="modificationcontact" /> <input type="hidden"
											name="idcontactamodifier"
											value="<c:out value="${count.index}"/>" />
									</form>
								</td>
							</tr>
						</c:forEach>
					</table>
				</div>
			</c:when>
		</c:choose>
	</div>



</body>
</html>