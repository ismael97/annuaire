<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<link href="styles/main.css" rel="stylesheet">
<head>
<meta charset="UTF-8">
<title>Menu.jsp</title>

</head>

<body>
	<div class="background">
	

	<div class="menu_bar">
		<ul>
			<li><a href="#ajouter">Ajouter</a>
			<li><a href="#contacts">Contacts</a>
			<li><a href="#rechercher">rechercher</a>

		</ul>
	</div>

	<div class="ajouter" id="ajouter">
		<form class="form2" action="http://localhost:8080/annuairepw/Acceuil"
			method="post">
			<div class="wrapper">
				<div>
					<input class ="element" type="text" name="nom" placeholder="Nom" class="element" />
				</div>

				<div>
					<input type="text" name="prenom" placeholder="Prenom"
						class="element" required />


				</div>

				<div>
					<input type="tel" name="num" placeholder="Numéro de téléphone"
						class="element" required />

				</div>
				<div>
					<input type="email" name="email" placeholder="Adresse Mail"
						class="element" />
				</div>

				<div>

					<input type="text" name="commentaire" placeholder="commentaire"
						class="element" />
				</div>



				<div>
					<input type="text" name="adresse" placeholder="adresse"
						class="element" />

				</div>


				<div>
					<input class ="submit" type="submit" value="Envoyer formulaire" class="submit" />

				</div>


				<div>
					<input type="hidden" name="typerequete" value="creationcontact" />
				</div>





			</div>
		</form>
	</div>

	<div class="recherche" id="rechercher">
		<div class="research">
			<form class="form1" action="http://localhost:8080/annuairepw/Acceuil"
				method="post">

				<input type="text" name="filtre" placeholder="rechercher un contact"
					class="element" /> <input type="hidden" name="typerequete"
					value="recherchecontact" /> <select name="typerecherche">
					<option selected>tous
					<option>nom
					<option>prenom
					<option>num
					<option>adresse
					<option>commentaire
					<option>email
				</select>
				<button>Search</button>



			</form>
		</div>

	</div>



	<c:set var="contactsutilisateur" value="${annuaire.getMonannuaire()}"
		scope="request" />

	<div class="contacts" id="contacts">

		<c:choose>
			<c:when test="${empty contactsutilisateur}">
				<p>La liste de contact est vide</p>
			</c:when>
			<c:when test="${!empty contactsutilisateur}">



				<table>
					<tr>
						<th>index</th>
						<th>Nom</th>
						<th>Prenom</th>
						<th>Numéro</th>
						<th>Adresse</th>
						<th>Email</th>
						<th>Commentaire</th>

					</tr>
					<c:forEach items="${contactsutilisateur}" var="contact"
						varStatus="count">
						<tr>
							<td><c:out value="${count.count}" /></td>
							<td><c:out value="${contact.getNom()}"></c:out></td>
							<td><c:out value="${contact.getPrenom()}"></c:out></td>
							<td><c:out value="${contact.getNum()}"></c:out></td>
							<td><c:out value="${contact.getAdresse()}"></c:out></td>
							<td><c:out value="${contact.getEmail()}"></c:out></td>
							<td><c:out value="${contact.getCommentaire()}"></c:out></td>
							<td>
								<form action="http://localhost:8080/annuairepw/Acceuil"
									method="post">
									<input type="submit" value="x" /> <input type="hidden"
										name="nom" value="<c:out value="${contact.getNom()}"></c:out>" />
									<input type="hidden" name="prenom"
										value="<c:out value="${contact.getPrenom()}"></c:out>" /> <input
										type="hidden" name="num"
										value="<c:out value="${contact.getNum()}"></c:out>" /> <input
										type="hidden" name="adresse"
										value="<c:out value="${contact.getAdresse()}"></c:out> " /> <input
										type="hidden" name="email"
										value="<c:out value="${contact.getEmail()}"></c:out>" /> <input
										type="hidden" name="commentaire"
										value="<c:out value="${contact.getCommentaire()}"></c:out>" />
									<input type="hidden" name="typerequete"
										value="supressioncontact" /> <input type="hidden"
										name="idcontactasupprimer"
										value="<c:out value="${count.index}"/>" />
								</form>
							</td>
							<td>
								<form action="http://localhost:8080/annuairepw/Acceuil"
									method="post">
									<input type="submit" value="modifier" /> <input type="hidden"
										name="nom" value="<c:out value="${contact.getNom()}"></c:out>" />
									<input type="hidden" name="prenom"
										value="<c:out value="${contact.getPrenom()}"></c:out>" /> <input
										type="hidden" name="num"
										value="<c:out value="${contact.getNum()}"></c:out>" /> <input
										type="hidden" name="adresse"
										value="<c:out value="${contact.getAdresse()}"></c:out> " /> <input
										type="hidden" name="email"
										value="<c:out value="${contact.getEmail()}"></c:out>" /> <input
										type="hidden" name="commentaire"
										value="<c:out value="${contact.getCommentaire()}"></c:out>" />
									<input type="hidden" name="typerequete"
										value="modificationcontact" /> <input type="hidden"
										name="idcontactamodifier"
										value="<c:out value="${count.index}"/>" />
								</form>
							</td>
						</tr>
					</c:forEach>
				</table>

			</c:when>
		</c:choose>
	</div>

	</div>

</body>
</html>