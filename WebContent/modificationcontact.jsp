<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<link href="styles/main.css" rel="stylesheet">
<meta charset="UTF-8">
<title>Confirmation ajout</title>
</head>
<body>
	<div class="menu_bar">
		<ul>
			<li><a href="http://localhost:8080/annuairepw/Acceuil">Acceuil</a>
		</ul>
	</div>




	<div class="ajouter">
		<form class="form2" action="http://localhost:8080/annuairepw/Acceuil"
			method="post">

			<div class="wrapper">

				<div>
					<input class="element"
						value="<c:out value="${contactamodifier.getNom()}"/>" type="text"
						name="nom" placeholder="Nom" class="element" />
				</div>

				<div>
					<input class="element"
						value="<c:out value="${contactamodifier.getPrenom()}"/>"
						type="text" name="prenom" placeholder="Prenom" class="element"
						required />
				</div>

				<div>
					<input class="element"
						value="<c:out value="${contactamodifier.getNum()}"/>" type="tel"
						name="num" placeholder="Numéro de téléphone" class="element"
						required />

				</div>
				<div>
					<input class="element"
						value="<c:out value="${contactamodifier.getEmail()}"/>"
						type="email" name="email" placeholder="Adresse Mail"
						class="element" />
				</div>

				<div>
					<input class="element"
						value="<c:out value="${contactamodifier.getCommentaire()}"/>"
						type="text" name="commentaire" placeholder="commentaire"
						class="element" />
				</div>

				<div>
					<input class="element"
						value="<c:out value="${contactamodifier.getAdresse()}"/>"
						type="text" name="adresse" placeholder="adresse" class="element" />
				</div>


				<!-- 		Anciennes informations qui serviront a supprimer l'ancien contact -->
				<input type="hidden"
					value="<c:out value="${contactamodifier.getNom()}"/>" type="text"
					name="oldnom" placeholder="Nom" class="element" /><br> <input
					type="hidden"
					value="<c:out value="${contactamodifier.getPrenom()}"/>"
					type="text" name="oldprenom" placeholder="Prenom" class="element"
					required /> <input type="hidden"
					value="<c:out value="${contactamodifier.getNum()}"/>" type="tel"
					name="oldnum" placeholder="Numéro de téléphone" class="element"
					required /> <input type="hidden"
					value="<c:out value="${contactamodifier.getEmail()}"/>"
					type="email" name="oldemail" placeholder="Adresse Mail"
					class="element" /> <input type="hidden"
					value="<c:out value="${contactamodifier.getCommentaire()}"/>"
					type="text" name="oldcommentaire" placeholder="commentaire"
					class="element" /> <input type="hidden"
					value="<c:out value="${contactamodifier.getAdresse()}"/>"
					type="text" name="oldadresse" placeholder="adresse" class="element" />


				<input type="hidden" name="typerequete" value="validermodification" />
				<input type="hidden" name="indexcontactamodifier"
					value="<c:out value="${contactamodifier.getNum()}"/>" /> <input
					class="submit" type="submit" />
			</div>
		</form>
	</div>




</body>
</html>